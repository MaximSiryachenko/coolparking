﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Interfaces
{
    public interface ILogService
    {
        string LogPath  { get; }
        void Write(string logInfo);
        string Read();
    }
}
