﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Interfaces
{
        using System.Timers;
        public interface    ITimerService
        {
            event ElapsedEventHandler Elapsed;
            double Interval { get; set; }
            void Start();
            void Stop();
            void Dispose();
        }
}
