﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

// TODO: реализовать класс ParkingService из интерфейса IParkingService.
//       Для попытки добавить автомобиль на полной парковке должно быть сгенерировано исключение InvalidOperationException.
//       Для попытки удалить транспортное средство с отрицательным балансом (долгом) должно быть выброшено InvalidOperationException.
//       Другие правила проверки и формат конструктора взяты из тестов.
//       Другие детали реализации зависят от вас, они просто должны соответствовать требованиям интерфейса
//       и тесты, например, в ParkingServiceTests вы можете найти необходимый формат конструктора и правила проверки.

using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Threading;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Timers;
using System.Transactions;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService, IDisposable
    {
        ITimerService billingTimer, logTimer;
        ILogService logService;

        public ParkingService()
        { 
            Parking p = new Parking();
            logService = new LogService();
            billingTimer = new TimerService();
            logTimer = new TimerService();
            billingTimer.Interval = Settings.BillingPeriod;
            logTimer.Interval = Settings.LoggingPeriod;
            billingTimer.Elapsed += Billing;
            billingTimer.Start();
            logTimer.Elapsed += WriteToLog;
            logTimer.Start();
        }


        public ParkingService(ITimerService billingTimer, ITimerService logTimer, ILogService logService)
        {
            Parking p = new Parking();
            this.logService = logService;
            this.billingTimer = billingTimer;
            this.logTimer = logTimer;
            this.billingTimer.Interval = Settings.BillingPeriod;
            this.logTimer.Interval = Settings.LoggingPeriod;
            this.billingTimer.Elapsed += Billing;
            this.billingTimer.Start();
            this.logTimer.Elapsed += WriteToLog;
            this.logTimer.Start();
        }


        public decimal GetBalance()
        {
            decimal balance = 0;
            foreach (TransactionInfo t in Parking.Transactions)
            {
                balance += t.Sum;
            }
            return balance;
        }

        public int GetCapacity()
        {
            return Settings.Capacity;
        }

       public int GetFreePlaces()
       {
           return Settings.Capacity - Parking.Vehicles.Count;
       }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            ReadOnlyCollection <Vehicle> v =  Parking.Vehicles.AsReadOnly();
            return v;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            Vehicle v = Parking.Vehicles.Find((item) => item.Id == vehicle.Id);
            if (v != null)
                throw new ArgumentException();
            Parking.Vehicles.Add(vehicle);

        }

        public void RemoveVehicle(string vehicleId)
        {
            Vehicle v = Parking.Vehicles.Find((item) => item.Id == vehicleId);
            if (v == null)
                throw new ArgumentException();
            Parking.Vehicles.Remove(v);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            Vehicle v = Parking.Vehicles.Find((item) => item.Id == vehicleId);
            if (sum <= 0 || v == null)
                throw new ArgumentException();
            v.Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Parking.Transactions.ToArray();
        }

        public void Billing(object sender, ElapsedEventArgs e)
        {
            for (int i = 0; i < Parking.Vehicles.Count; i++)
            {
                Parking.Vehicles[i].Balance -= Debit(Parking.Vehicles[i]);
                TransactionInfo t = new TransactionInfo();
                t.Id = Parking.Vehicles[i].Id;
                t.Time = DateTime.Now;
                t.Sum = Debit(Parking.Vehicles[i]);
                Parking.Transactions.Add(t);
            }
        }

        decimal Debit(Vehicle v)
        {
            if (v.Balance >= Settings.tariff[(int)v.VehicleType])
            {
                return Settings.tariff[(int)v.VehicleType];
            }
            else
            {
                return ((Settings.tariff[(int)v.VehicleType] - v.Balance)*Settings.Fine) + v.Balance;
            }
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void WriteToLog(object sender, ElapsedEventArgs e)
        {
            foreach (TransactionInfo t in Parking.Transactions)
            {
                logService.Write(Convert.ToString(t.Time) + " - " + Convert.ToString(t.Id) + " - " + Convert.ToString(t.Sum));
            }
            //Parking.Transactions.Clear();

        }

        public void Dispose()
        {
            
        }
    }
}
