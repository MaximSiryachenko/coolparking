﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Services
{
    using System.Timers;
    using CoolParking.BL.Interfaces;
    using CoolParking.BL.Models;
    public class TimerService : ITimerService
    {
        public double Interval { get; set; }

        private Timer Timer;

        public event ElapsedEventHandler Elapsed;

        public void Start()
        {
            Timer = new Timer(Interval);
            Timer.Elapsed += Elapsed;
            Timer.AutoReset = true;
            Timer.Enabled = true;
        }
        public void Stop()
        {
            Timer.AutoReset = false;
            Timer.Enabled = false;
            Dispose();
        }

        public void Dispose()
        {
            Timer.Dispose();
        }
    }
}
