﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.



using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    class Parking
    {
        private static Parking instance;

        public static List<Vehicle> Vehicles;
        public static List<TransactionInfo> Transactions;

        public Parking()
        {
            Vehicles = new List<Vehicle>();
            Transactions = new List<TransactionInfo>();
        }

        public static Parking getInstance()
        {
            if (instance == null)
                instance = new Parking();
            return instance;
        }

        //public static List<Vehicle> Vehicles = new List<Vehicle>();
        //public static List<TransactionInfo> Transactions = new List<TransactionInfo>();

    }
}
